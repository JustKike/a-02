const assert = require('chai').assert;
const app = require('../app');

crearResult = app.crearInventario();
pageResult = app.paginate(app.crearInventario(), 5, 1);
agregarResult = app.agregarProducto(app.crearInventario(), "Zapatos", 500, 6);

describe('App', function() {

    describe('crearInventario()', function() {
        it('Deberia mostrar un array de 15 elementos', function() {
            assert.lengthOf(crearResult, 15);
        });
        it('Deberia mostrar la pagina 1 con cinco elementos', function() {
            assert.lengthOf(pageResult, 5);
        });
        it('Deberia incrementar a 16 productos despues de agregar uno nuevo', function() {
            assert.lengthOf(agregarResult, 16);
        });
    });

});

//comando para ejecutar prueba: npm run test