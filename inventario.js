var inventario = [
    // { producto: "articulo 1", precio: 20, cantidad: 10 },
    // { producto: "articulo 2", precio: 20, cantidad: 10 },
    // { producto: "articulo 3", precio: 20, cantidad: 10 },
    // { producto: "articulo 4", precio: 20, cantidad: 10 },
    // { producto: "articulo 5", precio: 20, cantidad: 10 },
    // { producto: "articulo 6", precio: 20, cantidad: 10 },
    // { producto: "articulo 7", precio: 20, cantidad: 10 },
    // { producto: "articulo 8", precio: 20, cantidad: 10 },
    // { producto: "articulo 9", precio: 20, cantidad: 10 },
    // { producto: "articulo 10", precio: 20, cantidad: 10 },
    // { producto: "articulo 11", precio: 20, cantidad: 10 },
    // { producto: "articulo 12", precio: 20, cantidad: 10 },
    // { producto: "articulo 13", precio: 20, cantidad: 10 },
    // { producto: "articulo 14", precio: 20, cantidad: 10 },
    // { producto: "articulo 15", precio: 20, cantidad: 10 }
];

function crearInventario() {
    for (let i = 0; i < 15; i++) {
        agregarProducto("articulo " + (i + 1), Math.floor(Math.random() * 200), Math.floor(Math.random() * 100));
    }
}

function paginate(array, page_size, page_number) {
    --page_number; // because pages logically start with 1, but technically with 0
    return array.slice(page_number * page_size, (page_number + 1) * page_size);
}

function agregarProducto(name, precio, qty) {
    inventario.push({ producto: name, precio: precio, cantidad: qty });
}

function mostrarCatalogo() {
    let totalProd = inventario.length;
    if (totalProd > 0 && totalProd % 5 == 0) {
        console.log("\nInventario lista de 5 elementos por pagina");
        outer: for (let i = 1; i <= (totalProd / 5); i++) {
            console.log("Pagina: ", i)
            console.log(paginate(inventario, 5, i));
        }
    } else {
        console.log("Inventario lista de 5 elementos por pagina");
        outer: for (let i = 1; i <= (totalProd / 5) + 1; i++) {
            console.log("Pagina: ", i)
            console.log(paginate(inventario, 5, i));
        }
    }
}

function mostrarPagina(p) {
    console.log("\nFiltrado por pagina");
    let totalProd = inventario.length;
    if (totalProd > 0 && totalProd % 5 == 0) {
        if (totalProd > 0 && p <= (totalProd / 5)) {
            console.log("Catalogo de productos");
            console.log("Pagina: ", p);
            console.log(paginate(inventario, 5, p));
        } else {
            console.log("No existe esa pagina!");
        }
    } else {
        if (totalProd > 0 && p <= (totalProd / 5) + 1) {
            console.log("Catalogo de productos");
            console.log("Pagina: ", p);
            console.log(paginate(inventario, 5, p));
        } else {
            console.log("No existe esa pagina!");
        }
    }
}

function ultimoProd() {
    console.log("\nUltimos productos agregados")
    let totalProd = inventario.length;
    if (totalProd > 0 && totalProd % 5 == 0) {
        console.log("Pagina: ", totalProd / 5);
        console.log(paginate(inventario, 5, totalProd / 5));
    } else {
        console.log("Pagina: ", Number(((totalProd / 5) + 1).toFixed()));
        console.log(paginate(inventario, 5, (totalProd / 5)));
    }
}



crearInventario(); //crea 15 articulos y los agrega al inventario
mostrarCatalogo(); //nos muestra el catalogo
agregarProducto("Zapatos", 500, 6); //agregamos un producto al inventario
ultimoProd(); //nos muestra la ultima pagina
// // mostrarPagina(2); //elegimos una pagina para buscar