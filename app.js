module.exports = {
    crearInventario: function() {
        var inventario = [];
        for (let i = 0; i < 15; i++) {
            inventario.push({ producto: "articulo " + (i + 1), precio: Math.floor(Math.random() * 200), cantidad: Math.floor(Math.random() * 100) });
        }
        return inventario;
    },
    paginate: function(array, page_size, page_number) {
        --page_number; // because pages logically start with 1, but technically with 0
        return array.slice(page_number * page_size, (page_number + 1) * page_size);
    },
    agregarProducto: function(array, name, precio, qty) {
        array.push({ producto: name, precio: precio, cantidad: qty });
        return array;
    }
}


/*
function agregarProducto(name, precio, qty) {
    inventario.push({ producto: name, precio: precio, cantidad: qty });
}

*/